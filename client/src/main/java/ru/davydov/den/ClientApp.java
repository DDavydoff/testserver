package ru.davydov.den;


import ru.davydov.den.protocol.Command;
import ru.davydov.den.protocol.Result;
import ru.davydov.den.protocol.Serializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.stream.Collectors;

import static java.lang.System.out;
import static java.util.stream.IntStream.range;
import static ru.davydov.den.protocol.Serializer.serialize;

public class ClientApp {

    public static final int PORT = 7777;

    public static final int HOST_ARG_INDEX = 0;
    public static final int COMMAND_ARG_INDEX = 1;
    public static final int KEY_ARG_INDEX = 2;

    public static final int MIN_VALID_ARGUMENTS = 3;

    public static final String NOT_VALID_MSG = "<Неверная команда или параметры запроса>";

    public static final String ADD_COMMAND = "add";
    public static final String DELETE_COMMAND = "delete";
    public static final String GET_COMMAND = "get";

    public static void main(String[] args) {
        if (args.length >= MIN_VALID_ARGUMENTS) {
            Command command = createCommand(args);
            if (validateCommand(command)) {
                processCommand(args[HOST_ARG_INDEX], command);
            }
        } else {
            out.println(NOT_VALID_MSG);
        }
    }

    private static void processCommand(String host, Command command) {
        try (Socket socket = new Socket(host, PORT);
             OutputStream outputStream = socket.getOutputStream();
             InputStream inputStream = socket.getInputStream();
             PrintWriter printWriter = new PrintWriter(outputStream);
             InputStreamReader inputRead = new InputStreamReader(inputStream);
             BufferedReader bufferedReader = new BufferedReader(inputRead)) {
            sendCommand(command, printWriter);
            receiveResult(bufferedReader);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void receiveResult(BufferedReader bufferedReader) throws IOException {
        String data = bufferedReader.readLine();
        if (data != null) {
            Result result = Serializer.deserialize(data, Result.class);
            if (result != null) {
                if (!result.getMessage().isEmpty()) {
                    out.println(result.getMessage());
                } else if (!result.getValues().isEmpty()) {
                    result.getValues().forEach(out::println);
                }
            }
        }
    }

    private static void sendCommand(Command command, PrintWriter printWriter) throws com.fasterxml.jackson.core.JsonProcessingException, java.io.UnsupportedEncodingException {
        printWriter.write(serialize(command));
        printWriter.write('\n');
        printWriter.flush();
    }

    private static boolean validateCommand(Command command) {
        return (ADD_COMMAND.equals(command.getCommand()) && !command.getValues().isEmpty())
                || (DELETE_COMMAND.equals(command.getCommand()) && !command.getValues().isEmpty())
                || (GET_COMMAND.equals(command.getCommand()));
    }

    private static Command createCommand(String[] args) {
        String commandStr = args[COMMAND_ARG_INDEX].toLowerCase();
        String keyStr = args[KEY_ARG_INDEX];
        Command command = new Command(commandStr, keyStr);
        if (args.length > MIN_VALID_ARGUMENTS) {
            command.setValues(range(MIN_VALID_ARGUMENTS, args.length)
                    .mapToObj(i -> args[i])
                    .collect(Collectors.toList()));

        }
        return command;
    }
}
