package ru.davydov.den.protocol;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;

import static java.util.Collections.singletonList;

public class SerializerTest extends Assert{

    public static final String COMMAND_SERIALIZE_RES = "{\"command\":\"add\",\"key\":\"hello\",\"values\":[\"привет\"]}";
    public static final Command COMMAND = new Command("add", "hello", singletonList("привет"));

    public static final Result RESULT = new Result("msg", new HashSet<>(singletonList("test")));
    public static final String RESULT_SERIALIZE_RES = "{\"message\":\"msg\",\"values\":[\"test\"]}";

    @Test
    public void serializeCommandTest()
            throws UnsupportedEncodingException, JsonProcessingException {
        String result = Serializer.serialize(COMMAND);
        assertEquals(COMMAND_SERIALIZE_RES, result);
    }

    @Test
    public void serializeResultTest()
            throws UnsupportedEncodingException, JsonProcessingException {
        String result = Serializer.serialize(RESULT);
        assertEquals(RESULT_SERIALIZE_RES, result);
    }

    @Test
    public void deserializeCommandTest() throws IOException {
        Command command = Serializer.deserialize(COMMAND_SERIALIZE_RES, Command.class);
        assertEquals(COMMAND, command);
    }

    @Test
    public void deserializeResultTest() throws IOException {
        Result result = Serializer.deserialize(RESULT_SERIALIZE_RES, Result.class);
        assertEquals(RESULT, result);
    }

}
