package ru.davydov.den.protocol;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class Serializer {

    public static <T> String serialize(T result) throws JsonProcessingException, UnsupportedEncodingException {
        ObjectMapper mapper = new ObjectMapper();
        return new String(mapper.writeValueAsBytes(result), "UTF-8");
    }

    public static <T> T deserialize(String input, Class<T> resClass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(input, resClass);
    }

}
