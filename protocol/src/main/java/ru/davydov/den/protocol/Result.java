package ru.davydov.den.protocol;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Result {

    @JsonProperty(value = "message", defaultValue = "", required = true)
    private String message;

    @JsonProperty(value = "values")
    private Set<String> values;

    public Result(String message) {
        this(message, new HashSet<>());
    }

    public Result(Set<String> values) {
        this("", values);
    }

    public Result(@JsonProperty(value = "message", defaultValue = "", required = true) String message,
                  @JsonProperty(value = "values") Set<String> values) {
        this.message = message;
        this.values = values;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Set<String> getValues() {
        return values;
    }

    public void setValues(Set<String> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "Result{" +
                "message='" + message + '\'' +
                ", values=" + values +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Result result = (Result) o;

        if (message != null ? !message.equals(result.message) : result.message != null) return false;
        return values != null ? values.equals(result.values) : result.values == null;
    }

    @Override
    public int hashCode() {
        int result = message != null ? message.hashCode() : 0;
        result = 31 * result + (values != null ? values.hashCode() : 0);
        return result;
    }
}
