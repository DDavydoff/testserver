package ru.davydov.den.protocol;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Command {

    @JsonProperty(value = "command", required = true)
    private String command;

    @JsonProperty(value = "key", required = true)
    private String key;

    @JsonProperty(value = "values")
    private List<String> values;

    public Command(@JsonProperty(value = "command", required = true) String command,
                   @JsonProperty(value = "key", required = true) String key,
                   @JsonProperty(value = "values") List<String> values) {
        this.command = command;
        this.key = key;
        this.values = values;
    }

    public Command(String command, String key) {
        this(command, key, new ArrayList<>());
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "Command{" +
                "command='" + command + '\'' +
                ", key='" + key + '\'' +
                ", values=" + values +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Command command1 = (Command) o;

        if (!command.equals(command1.command)) return false;
        if (!key.equals(command1.key)) return false;
        return values != null ? values.equals(command1.values) : command1.values == null;
    }

    @Override
    public int hashCode() {
        int result = command.hashCode();
        result = 31 * result + key.hashCode();
        result = 31 * result + (values != null ? values.hashCode() : 0);
        return result;
    }
}
