package ru.davydov.den;


import ru.davydov.den.protocol.Command;
import ru.davydov.den.protocol.Result;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.String.format;

public class CommandHandler {

    public static final String ADD_COMMAND = "add";
    public static final String GET_COMMAND = "get";
    public static final String DELETE_COMMAND = "delete";

    public static final String GET_FAIL_MSG = "<слово отсутвует в словаре>";
    public static final String DELETE_FAIL_MSG = "<слово/значение отсутвует в словаре>";
    public static final String DELETE_SUCCESS_MSG = "<значения слова успешно удалены>";
    public static final String ADD_SUCCESS_MSG = "<значения слова успешно добавлены>";

    public static final Result EMPTY_RESULT = new Result("<команда не поддерживается>");

    private final static Logger LOGGER = Logger.getLogger(CommandHandler.class.getName());
    public static final String LOGGER_TEMPLATE_STR = "%s: %s";

    public static Result execute(Command command) {
        if (ADD_COMMAND.equals(command.getCommand())) {
            return add(command);
        } else if (GET_COMMAND.equals(command.getCommand())) {
            return get(command);
        } else if (DELETE_COMMAND.equals(command.getCommand())) {
            return delete(command);
        }
        return EMPTY_RESULT;
    }

    private static Result delete(Command command) {
        try {
            Storage.getInstance().delete(command.getKey(), new HashSet<>(command.getValues()));
            LOGGER.finest(format(LOGGER_TEMPLATE_STR, DELETE_SUCCESS_MSG, command.toString()));
            return new Result(DELETE_SUCCESS_MSG);
        } catch (Storage.DictionaryException e) {
            LOGGER.fine(format(LOGGER_TEMPLATE_STR, DELETE_FAIL_MSG, command.toString()));
            return new Result(DELETE_FAIL_MSG);
        }
    }

    private static Result get(Command command) {
        try {
            Set<String> values = Storage.getInstance().get(command.getKey());
            LOGGER.finest(format("Command %s", command.toString()));
            Result result = new Result(values);
            LOGGER.finest(format("Result %s", result.toString()));
            return result;
        } catch (Storage.DictionaryException e) {
            LOGGER.fine(format(LOGGER_TEMPLATE_STR, GET_FAIL_MSG, command.toString()));
            return new Result(GET_FAIL_MSG);
        }
    }

    private static Result add(Command command) {
        Storage.getInstance().add(command.getKey(), new HashSet<>(command.getValues()));
        LOGGER.finest(format(LOGGER_TEMPLATE_STR, ADD_SUCCESS_MSG, command.toString()));
        return new Result(ADD_SUCCESS_MSG);
    }
}
