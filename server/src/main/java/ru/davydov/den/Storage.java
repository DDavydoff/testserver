package ru.davydov.den;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

public class Storage {

    private volatile static Storage _instance;

    private final Map<String, Set<String>> dictionary;

    private Storage() {
        dictionary = new ConcurrentHashMap<>();
    }

    public static Storage getInstance() {
        Storage localInstance = _instance;
        if (localInstance == null) {
            synchronized (Storage.class) {
                localInstance = _instance;
                if (localInstance == null) {
                    _instance = localInstance = new Storage();
                }
            }
        }
        return localInstance;
    }

    public void add(String key, final Set<String> values) {
        synchronized (dictionary) {
            if (!dictionary.containsKey(key)) {
                dictionary.put(key, new CopyOnWriteArraySet<>());
            }
            dictionary.get(key).addAll(values);
        }
    }

    public Set<String> get(String key) throws DictionaryException {
        if (dictionary.containsKey(key)) {
            return dictionary.get(key);
        }
        throw new DictionaryException();
    }

    public void delete(String key, final Set<String> values) throws DictionaryException {
        if (dictionary.containsKey(key)) {
            if (dictionary.get(key).removeAll(values)) {
                return;
            }
        }
        throw new DictionaryException();
    }

    public void clear() {
        dictionary.clear();
    }

    public static class DictionaryException extends Exception {
        public DictionaryException() {
            super();
        }
    }
}
