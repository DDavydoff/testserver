package ru.davydov.den;

import ru.davydov.den.protocol.Command;
import ru.davydov.den.protocol.Result;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import static java.lang.String.format;
import static ru.davydov.den.protocol.Serializer.deserialize;
import static ru.davydov.den.protocol.Serializer.serialize;


public class App {

    public static final int PORT = 7777;

    private static ExecutorService executorService = Executors.newCachedThreadPool();

    private final static Logger LOGGER = Logger.getLogger(App.class.getName());

    public static void main(String[] args) {
        try (ServerSocket server = new ServerSocket(PORT)) {
            LOGGER.info(format("Listen port %d", PORT));
            while (true) {
                Socket socket = server.accept();
                executorService.execute(() -> connectionProcess(socket));
            }
        } catch (IOException e) {
            LOGGER.severe(format("Start fail. Exception: %s", e.getMessage()));
        } finally {
            executorService.shutdown();
        }
    }

    private static void connectionProcess(Socket socket) {
        try (final InputStream inputStream = socket.getInputStream();
             final OutputStream outputStream = socket.getOutputStream();
             final InputStreamReader inputRead = new InputStreamReader(inputStream);
             final BufferedReader bufferedReader = new BufferedReader(inputRead);
             final PrintWriter printWriter = new PrintWriter(outputStream)) {
            LOGGER.info("Incoming connection");
            String data = bufferedReader.readLine();
            if (data != null) {
                Command command = deserialize(data, Command.class);
                if (validateCommand(command)) {
                    Result result = CommandHandler.execute(command);
                    sendResult(printWriter, result);
                }
            } else {
                LOGGER.warning("Can't read");
            }
        } catch (IOException e) {
            LOGGER.warning(format("Socket exception: %s", e.getMessage()));
        }
    }

    static boolean validateCommand(Command command) {
        return (CommandHandler.ADD_COMMAND.equals(command.getCommand()) && validateAddCommand(command))
                || (CommandHandler.GET_COMMAND.equals(command.getCommand()) && validateGetCommand(command))
                || (CommandHandler.DELETE_COMMAND.equals(command.getCommand()) && validateDeleteCommand(command));
    }

    private static boolean validateGetCommand(Command command) {
        return !command.getKey().isEmpty();
    }

    private static boolean validateDeleteCommand(Command command) {
        return !command.getKey().isEmpty() && command.getValues() != null && !command.getValues().isEmpty();
    }

    private static boolean validateAddCommand(Command command) {
        return !command.getKey().isEmpty() && (command.getValues() != null && !command.getValues().isEmpty());
    }

    private static void sendResult(PrintWriter printWriter, Result result) {
        try {
            LOGGER.info("Send result");
            printWriter.write(serialize(result));
            printWriter.write("\n");
            printWriter.flush();
        } catch (IOException e) {
            LOGGER.warning(format("Answer exception: %s", e.getMessage()));
        }
    }
}
