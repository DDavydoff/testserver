package ru.davydov.den;

import org.junit.Assert;
import org.junit.Test;
import ru.davydov.den.protocol.Command;
import ru.davydov.den.protocol.Result;

import java.util.HashSet;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;


public class CommandHandlerTest extends Assert {

    public static final String VALUE1 = "привет";
    public static final String VALUE2 = "здравствуйте";

    public static final String KEY = "hello";
    public static final String NOT_VALID_KEY = "not_valid";
    public static final String VALUE_NOT_VALID = "test_not_valid";

    @Test
    public void addTest() {
        Result result = CommandHandler.execute(new Command(CommandHandler.ADD_COMMAND, KEY, singletonList(VALUE1)));
        assertEquals(CommandHandler.ADD_SUCCESS_MSG, result.getMessage());
    }

    @Test
    public void addTest2() {
        Result result = CommandHandler.execute(new Command(CommandHandler.ADD_COMMAND, KEY, singletonList(VALUE2)));
        assertEquals(CommandHandler.ADD_SUCCESS_MSG, result.getMessage());
    }

    @Test
    public void getTest() {
        CommandHandler.execute(new Command(CommandHandler.ADD_COMMAND, KEY, asList(VALUE1, VALUE2)));
        Result result = CommandHandler.execute(new Command(CommandHandler.GET_COMMAND, KEY));
        assertEquals(new HashSet<>(asList(VALUE1, VALUE2)), result.getValues());
    }

    @Test
    public void deleteTest(){
        CommandHandler.execute(new Command(CommandHandler.ADD_COMMAND, KEY, asList(VALUE1, VALUE2)));
        Result result = CommandHandler.execute(new Command(CommandHandler.DELETE_COMMAND, KEY, singletonList(VALUE1)));
        assertEquals(CommandHandler.DELETE_SUCCESS_MSG, result.getMessage());
    }

    @Test
    public void getFailTest(){
        Result result = CommandHandler.execute(new Command(CommandHandler.GET_COMMAND, NOT_VALID_KEY));
        assertEquals(CommandHandler.GET_FAIL_MSG, result.getMessage());
    }

    @Test
    public void deleteFailTest1(){
        Result result = CommandHandler.execute(new Command(CommandHandler.DELETE_COMMAND, NOT_VALID_KEY,
                singletonList(VALUE_NOT_VALID)));
        assertEquals(CommandHandler.DELETE_FAIL_MSG, result.getMessage());
    }

    @Test
    public void deleteFailTest2(){
        Result result = CommandHandler.execute(new Command(CommandHandler.DELETE_COMMAND, KEY, singletonList(VALUE_NOT_VALID)));
        assertEquals(CommandHandler.DELETE_FAIL_MSG, result.getMessage());
    }

}
