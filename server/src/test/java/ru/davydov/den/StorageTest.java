package ru.davydov.den;


import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashSet;

import static java.util.Arrays.asList;

public class StorageTest extends Assert {

    public static final String KEY = "hello";
    public static final String VALUE1 = "привет";
    public static final String VALUE2 = "здравствуйте";

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void addTest() {
        Storage.getInstance().add(KEY, new HashSet<>(asList(VALUE1, VALUE2)));
    }

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void getTest(){
        try {
            Storage.getInstance().get(KEY);
        } catch (Storage.DictionaryException e) {
        }
    }

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void deleteTest(){
        try {
            Storage.getInstance().delete(KEY, new HashSet<>(asList(VALUE1, VALUE2)));
        } catch (Storage.DictionaryException e) {
        }
    }

}
