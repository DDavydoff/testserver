package ru.davydov.den;


import org.junit.Assert;
import ru.davydov.den.protocol.Command;

public class Test extends Assert{

    @org.junit.Test
    public void validateTest(){
        assertFalse(App.validateCommand(new Command(CommandHandler.ADD_COMMAND, "key")));
        assertFalse(App.validateCommand(new Command(CommandHandler.DELETE_COMMAND, "key")));
        assertTrue(App.validateCommand(new Command(CommandHandler.GET_COMMAND, "key")));
    }

}
